# libgc built with Zig

This project builds [libgc (bdwgc)](https://github.com/ivmai/bdwgc) with Zig. These are
_not Zig language bindings_ to the project. The goal of this project is to
enable the upstream project to be used with the Zig package manager. Downstream
users may also not be Zig language users, they may just be using Zig as a build
system.

## Usage

Create a `build.zig.zon` like so:

```zig
.{
    .name = "my-project",
    .version = "0.0.0",
    .dependencies = .{
        .libgc = .{
            .url = "https://codeberg.org/linusg/zig-build-libgc/archive/<git-ref-here>.tar.gz",
            .hash = "12208070233b17de6be05e32af096a6760682b48598323234824def41789e993432c",
        },
    },
}
```

And in your `build.zig`:

```zig
const libgc = b.dependency("libgc", .{ .target = target, .optimize = optimize });
exe.linkLibrary(libgc.artifact("gc"));
```

In your code you can now `@cImport` the project.
