const std = @import("std");

const build_macos_sdk = @import("macos_sdk");

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const bdwgc = b.dependency("bdwgc", .{});

    const flags_common = [_][]const u8{
        // NOTE: These two are set by default when building libgc with CMake
        // https://github.com/ivmai/bdwgc/blob/242a3a7b6040c2680e009367e96a77b0e167619a/CMakeLists.txt#L139
        "-DALL_INTERIOR_POINTERS",
        "-DNO_EXECUTE_PERMISSION",
        // NOTE: This allows linking against the musl libc, which doesn't implement getcontext().
        "-DNO_GETCONTEXT",
        // FIXME: libgc fails to recognize interior pointers somewhere when building in release mode,
        //        enabling this circumvents this bug until it is investigated and fixed properly.
        "-DGC_ASSERTIONS",
    };
    const flags_wasi = [_][]const u8{
        "-D__wasi__",
        "-D_WASI_EMULATED_SIGNAL",
        "-Wl,wasi-emulated-signal",
        // FIXME: This gets set to 4096 by default which then causes wasi-libc to randomly abort().
        //        Should probably be fixed upstream.
        "-DHBLKSIZE=65536",
    };
    const flags = if (target.result.os.tag == .wasi)
        &(flags_common ++ flags_wasi)
    else
        &flags_common;

    const lib = b.addStaticLibrary(.{
        .name = "gc",
        .target = target,
        .optimize = optimize,
    });
    lib.linkLibC();
    lib.addCSourceFiles(.{
        .root = bdwgc.path(""),
        .files = &.{
            // https://github.com/ivmai/bdwgc/blob/242a3a7b6040c2680e009367e96a77b0e167619a/CMakeLists.txt#L171-L174
            "allchblk.c",
            "alloc.c",
            "blacklst.c",
            "dbg_mlc.c",
            "dyn_load.c",
            "finalize.c",
            "headers.c",
            "mach_dep.c",
            "malloc.c",
            "mallocx.c",
            "mark_rts.c",
            "mark.c",
            "misc.c",
            "new_hblk.c",
            "obj_map.c",
            "os_dep.c",
            "ptr_chck.c",
            "reclaim.c",
            "typd_mlc.c",
        },
        .flags = flags,
    });
    lib.addIncludePath(bdwgc.path("include"));
    lib.installHeadersDirectoryOptions(.{
        .source_dir = bdwgc.path("include"),
        .install_dir = .header,
        .install_subdir = "",
        .include_extensions = &.{
            // https://github.com/ivmai/bdwgc/blob/242a3a7b6040c2680e009367e96a77b0e167619a/include/include.am#L15-L25
            "gc/gc.h",
            "gc/gc_backptr.h",
            "gc/gc_config_macros.h",
            "gc/gc_inline.h",
            "gc/gc_mark.h",
            "gc/gc_tiny_fl.h",
            "gc/gc_typed.h",
            "gc/gc_version.h",
            "gc/javaxfc.h",
            "gc/leak_detector.h",
        },
    });
    if (target.result.os.tag == .macos) {
        build_macos_sdk.addPaths(lib);
    }
    b.installArtifact(lib);
}
